import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import User from './user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

    constructor(private userService: UsersService){}
    
    @MessagePattern('user.create')
    async createUser(user:User){
        console.log(user);
        const createdUser = this.userService.create(user);
        return createdUser;
    }
}
